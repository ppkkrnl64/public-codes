ppk Public repository
====================


EN - This repository contains codes developed for years in projects or for pleasure.
It is organized by programming languages and licensed under Apache 2.0 license.

For more information about the released codes consult the file EN-Contents.txt

ES - Este repositorio contiene códigos desarrollados durante años en proyectos o por placer.
Está organizado por lenguajes de programación y licenciado bajo licencia Apache 2.0.

Para más información de los códigos liberados consulta el archivo ES-Contenido.txt

Pedro Buendia Palomino 2018
@plusplusk