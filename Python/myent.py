'''
Copyright 2018 Pedro Buendía Palomino - @plusplusk

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

File Entropy Script
@ppk

Script Skills:
+ Read great files in chunks
+ Calculate Shannon file entropy
'''

from __future__ import division
import sys
import os
import math


#default buffer size 8k
def get_chunks(file_size, chunk_size = 8192):

    chunk_start = 0
    while chunk_start + chunk_size < file_size:
        yield(chunk_start, chunk_size)
        chunk_start += chunk_size

    final_chunk_size = file_size - chunk_start
    yield(chunk_start, final_chunk_size)

    
def read_file_chunked(file_path):

    global fsize
    file_size = os.path.getsize(file_path)
    fsize = file_size
    
    with open(file_path,'rb') as file_:
              
        progress = 0

        for chunk_start, chunk_size in get_chunks(file_size):

            file_chunk = file_.read(chunk_size)
            
            # byte count
            for b in file_chunk:
                freqs[ord(b)]+= 1
                
            #uncomment to show file read progress    
            # progress += len(file_chunk)
            # print '{0} of {1} bytes read ({2}%)'.format( progress, file_size, int(progress / file_size * 100))
        
        #calculate the frequency of each byte
        for idx, f in enumerate(freqs):
            freqs[idx] = float(f / file_size)
        print
  
  
freqs = [0] * 256
fsize = 0

if len(sys.argv) != 2:
    print "Usage entropy [file]"
    sys.exit()
    
if __name__ == '__main__':

    read_file_chunked(sys.argv[1])
    
    # uncomment to print byte frequencies
    # for idx,f in enumerate(freqs):
        # print hex(idx),f
                        
    # calculate Shannon entropy
    ent = 0.0    
    for f in freqs:
        if f > 0:
            ent += f * math.log(f,2)
    ent = -ent
	
    print 'File size: ', fsize
    print
    print 'Shannon entropy ( min bits per byte-character )'
    print ent
    print
    print 'Min possible file size assuming max compression efficiency'
    print (ent * fsize), ' in bits'
    print (ent * fsize) / 8, ' in bytes'
    print (ent * fsize) / 8 / 1024, ' in Kbytes'
    print (ent * fsize) / 8 / 1024 / 1024, ' in Mbytes'